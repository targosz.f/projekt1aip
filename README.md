Moj skrypt:
    Usuwanie nadmiernych spacji, odpowiednia zamiana wielkosci liter oraz podkreslanie bledow
    Filip Targosz gr.4

Wykorzystanie:
    stedtxt.py <plik/text>                   Wprowadzenie pliku lub tekstu do przerobienia i zapisanie w pliku(domyslnie answer.txt) 

Opcje:
    --help          -h                       Wypisanie polecen 
    --in <plik/text wejsciowy>               Wprowadzenie pliku lub tekstu do przerobienia  
    --out <nazwa pliku wyjsciowego>          Zapisanie pliku w konkretnmym miejsciu(domyslnie answer.txt)
    --stat          -s                       Do pliku zostanie wpisane ilosc slow, linijek, znakow i bialych znakow
    --wypisz        -w                       Wypisuje przerobiony tekst w konsoli
    --wypiszbez     -wb                      Tekst dostanie tylko wypisany w konsoli bez zapisu
    --rev           -r                       Program zamienia tekst na jego wersje z dodatkowymi spajcami i losowa wielkoscia liter

Z pozdrowieniami dla profesora :)